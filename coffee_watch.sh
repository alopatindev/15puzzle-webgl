#!/bin/sh

while inotifywait -r -s -qq -e close_write $(pwd)
do
    coffee -c game.coffee
done
