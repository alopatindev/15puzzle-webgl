window.onload = ->
  gl = makeContext()
  if !gl
    window.location = "http://get.webgl.org/troubleshooting"
  else
    gl.canvas.onmousemove = (event) ->
      smooth = 0.8
      offset = 0.5
      angleX = (event.clientX / gl.canvas.width) - offset
      angleY = ((event.clientY * offset) / gl.canvas.height)
      requestAnimationFrame((time) ->
        rotateWorld(angleX * smooth, angleY * smooth)
        drawWorld()
      )

    backgroundImage = gl.makeImage("background", 0.0, 0.0)
    gentooImage = gl.makeImage("gentoo", 0.0, 0.0)

    projUniformLoc = gl.getUniformLocation(gl.program, "proj")
    viewUniformLoc = gl.getUniformLocation(gl.program, "view")
    worldUniformLoc = gl.getUniformLocation(gl.program, "world")

    newMatrix = ->
      new Float32Array(16)

    identityMat = newMatrix()
    projMat = newMatrix()
    viewMat = newMatrix()
    worldMat = newMatrix()
    xRotMat = newMatrix()
    yRotMat = newMatrix()

    rotateWorld = (angleX, angleY) ->
      mat4.rotate(xRotMat, identityMat, angleX, [0, 1, 0])
      mat4.rotate(yRotMat, identityMat, angleY, [1, 0, 0])
      mat4.mul(worldMat, xRotMat, yRotMat)

    mat4.identity(identityMat)
    mat4.perspective(projMat, glMatrix.toRadian(45), gl.canvas.width / gl.canvas.height, 0.1, 1000.0)
    mat4.lookAt(viewMat, [0, 0, -4], [0, 0, 0], [0, 1, 0])
    rotateWorld(0, 0)

    drawWorld = ->
      gl.uniformMatrix4fv(projUniformLoc, gl.FALSE, projMat)
      gl.uniformMatrix4fv(viewUniformLoc, gl.FALSE, viewMat)
      gl.uniformMatrix4fv(worldUniformLoc, gl.FALSE, worldMat)

      gl.clearColor(0.5, 0.5, 0.5, 1.0)
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)


      gl.enable(gl.DEPTH_TEST)
      #gl.enable(gl.CULL_FACE)
      #gl.frontFace(gl.CCW)
      #gl.cullFace(gl.BACK)

      backgroundImage
        .transform(0.0, 0.0, 14.0)
        .draw()

      gentooImage
        .transform(0.0, 0.0, -1.0)
        .draw()
        .transform(1.0, 1.0, -1.5)
        .draw()

    drawWorld()

makeContext = ->
  if !window.WebGLRenderingContext
    return
  else
    canvas = document.getElementById("c")
    console.log(canvas)
    gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl")
    if !gl
      return
    else
      gl.canvas = canvas

      gl.makeShader = (text, type) ->
        shader = gl.createShader(type)
        gl.shaderSource(shader, text)
        gl.compileShader(shader)
        if !gl.getShaderParameter(shader, gl.COMPILE_STATUS)
          console.error(gl.getShaderInfoLog(shader))
          return
        return shader

      gl.makeProgram = (vert, frag) ->
        vertexShader = gl.makeShader(vert, gl.VERTEX_SHADER)
        fragmentShader = gl.makeShader(frag, gl.FRAGMENT_SHADER)
        program = gl.createProgram()
        gl.attachShader(program, vertexShader)
        gl.attachShader(program, fragmentShader)
        gl.linkProgram(program)
        if !gl.getProgramParameter(program, gl.LINK_STATUS)
          console.error(gl.getProgramInfoLog(program))
          return
        gl.validateProgram(program)
        if !gl.getProgramParameter(program, gl.VALIDATE_STATUS)
          console.error(gl.getProgramInfoLog(program))
          return
        return program

      gl.program = gl.makeProgram(vertexShaderText, fragmentShaderText)
      gl.useProgram(gl.program)

      gl.loadTexture = (name) ->
        texture = gl.createTexture()
        gl.bindTexture(gl.TEXTURE_2D, texture)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
        gl.texImage2D(
            gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
            gl.UNSIGNED_BYTE,
            document.getElementById(name)
        )
        return texture

      gl.makeImage = (textureName, offsetU, offsetV) ->
        new Image(0.0, 0.0, gl.loadTexture(textureName), gl)

      return gl

class Image
  @vbo = null
  @texture = null
  @gl = null

  rect = (x, y, width) -> [
    x,y,       x+width,y,
    x,y+width, x+width,y+width
  ]

  constructor: (offsetU, offsetV, texture, gl) ->
    @texture = texture
    @gl = gl

    verts = rect(0.0, 0.0, -1.0)
    @vbo = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, @vbo)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.DYNAMIC_DRAW)
    posAttribLoc = gl.getAttribLocation(gl.program, "vertPos")
    gl.vertexAttribPointer(posAttribLoc, 2, gl.FLOAT, gl.FALSE, 0, 0)
    gl.enableVertexAttribArray(posAttribLoc)

    uv = rect(offsetU, offsetV, 1.0)
    uvVBO = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, uvVBO)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(uv), gl.STATIC_DRAW)
    posAttribLocUv = gl.getAttribLocation(gl.program, "vertTexCoord")
    gl.vertexAttribPointer(posAttribLocUv, 2, gl.FLOAT, gl.FALSE, 0, 0)
    gl.enableVertexAttribArray(posAttribLocUv)

  # TODO: support z
  transform: (x, y, z) ->
    width = -1.0
    @gl.bindBuffer(@gl.ARRAY_BUFFER, @vbo)
    @gl.bufferSubData(@gl.ARRAY_BUFFER, 0, new Float32Array(rect(x, y, width)))
    return this

  draw: ->
    @gl.bindTexture(@gl.TEXTURE_2D, @texture)
    @gl.activeTexture(@gl.TEXTURE0)
    @gl.drawArrays(@gl.TRIANGLE_STRIP, 0, 4)
    return this

vertexShaderText = """
precision mediump float;

attribute vec3 vertPos;
uniform mat4 proj;
uniform mat4 view;
uniform mat4 world;

attribute vec2 vertTexCoord;
varying vec2 fragTexCoord;

void main()
{
  fragTexCoord = vertTexCoord;
  gl_Position = proj * view * world * vec4(vertPos, 1.0);
}
"""

fragmentShaderText = """
precision mediump float;

varying vec2 fragTexCoord;
uniform sampler2D sampler;

void main()
{
  gl_FragColor = texture2D(sampler, fragTexCoord);
}
"""
